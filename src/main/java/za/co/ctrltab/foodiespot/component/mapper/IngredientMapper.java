package za.co.ctrltab.foodiespot.component.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import za.co.ctrltab.foodiespot.domain.Ingredient;
import za.co.ctrltab.foodiespot.persistence.entity.IngredientEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface IngredientMapper {
  IngredientMapper INSTANCE = Mappers.getMapper(IngredientMapper.class);

  Ingredient mapEntityToIngredient(IngredientEntity ingredientEntity);

  IngredientEntity mapIngredientToEntity(Ingredient ingredient);
}
