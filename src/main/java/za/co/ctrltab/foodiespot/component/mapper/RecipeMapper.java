package za.co.ctrltab.foodiespot.component.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import za.co.ctrltab.foodiespot.domain.Ingredient;
import za.co.ctrltab.foodiespot.domain.Recipe;
import za.co.ctrltab.foodiespot.persistence.entity.IngredientEntity;
import za.co.ctrltab.foodiespot.persistence.entity.RecipeEntity;
import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RecipeMapper {
  RecipeMapper INSTANCE = Mappers.getMapper(RecipeMapper.class);

  Recipe mapEntityToRecipe(RecipeEntity recipeEntity);

  RecipeEntity mapRecipeToEntity(Recipe recipe);

  List<IngredientEntity> mapIngredientsToEntities(List<Ingredient> ingredients);

  List<Ingredient> mapEntitiesToIngredients(List<IngredientEntity> ingredientEntities);
}
