package za.co.ctrltab.foodiespot.component.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import za.co.ctrltab.foodiespot.domain.Creator;
import za.co.ctrltab.foodiespot.persistence.entity.CreatorEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CreatorMapper {
  CreatorMapper INSTANCE = Mappers.getMapper(CreatorMapper.class);

  Creator mapEntityToCreator(CreatorEntity creatorEntity);

  CreatorEntity mapCreatorToEntity(Creator ingredient);
}
