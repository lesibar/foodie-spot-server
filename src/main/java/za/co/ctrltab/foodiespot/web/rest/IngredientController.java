package za.co.ctrltab.foodiespot.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.ctrltab.foodiespot.domain.Ingredient;
import za.co.ctrltab.foodiespot.service.IngredientService;
import java.util.List;

@RestController
@RequestMapping("/api/v1/ingredients")
@CrossOrigin("*")
public class IngredientController {

  @Autowired
  private IngredientService ingredientService;

  @GetMapping
  public List<Ingredient> getAllIngredients() {
    return ingredientService.getAllRecipes();
  }

}
