package za.co.ctrltab.foodiespot.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.ctrltab.foodiespot.domain.Ingredient;
import za.co.ctrltab.foodiespot.domain.Recipe;
import za.co.ctrltab.foodiespot.service.RecipeService;
import java.util.List;

@RestController
@RequestMapping("/api/v1/recipes")
@CrossOrigin("*")
public class RecipeController {

  @Autowired
  private RecipeService recipeService;

  @GetMapping
  public List<Recipe> getAllRecipes() {
    return recipeService.getAllRecipes();
  }

  @GetMapping("/{id}")
  public Recipe getRecipeById(@PathVariable final Long id) {
    return recipeService.findRecipeById(id);
  }

  @PutMapping("/{id}")
  public void updateRecipe(@PathVariable final Long id, @RequestBody final Recipe recipe) {
    recipeService.updateRecipe(id, recipe);
  }

  @PostMapping
  public void createRecipe(@RequestBody final Recipe recipe) {
    recipeService.createRecipe(recipe);
  }

  @DeleteMapping("/{id}")
  public void deleteRecipe(@PathVariable final Long id) {
    recipeService.deleteRecipe(id);
  }

  @GetMapping("/{id}/ingredients")
  public List<Ingredient> getRecipeIngredients(@PathVariable final Long id) {
    return recipeService.getRecipeIngredients(id);
  }

  @PostMapping("/{id}/ingredients")
  public void addIngredientToRecipe(@PathVariable final Long id,
      @RequestBody final Ingredient ingredient) {
    recipeService.addIngredientToRecipe(id, ingredient);
  }

  @DeleteMapping("/{id}/ingredients/{ingredientId}")
  public void removeIngredientFromRecipe(@PathVariable final Long id,
      @PathVariable final Long ingredientId) {
    recipeService.removeIngredientFromRecipe(id, ingredientId);
  }
}
