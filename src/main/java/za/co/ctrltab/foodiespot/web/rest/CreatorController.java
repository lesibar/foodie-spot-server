package za.co.ctrltab.foodiespot.web.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.ctrltab.foodiespot.domain.Creator;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/api/v1/creators/")
@CrossOrigin("*")
public class CreatorController {

  @GetMapping
  public List<Creator> getAllCreators() {
    List<Creator> creators = new ArrayList<>();

    IntStream.range(0, 5)
        .forEach((i) -> creators.add(Creator.builder().name(String.format("Test %d", i))
            .emailAddress(String.format("test%d.test@me.co.za", i)).numberOfContributions(i * 2)
            .id((long) i).build()));

    return creators;
  }
}
