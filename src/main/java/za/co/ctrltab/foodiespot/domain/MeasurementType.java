package za.co.ctrltab.foodiespot.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum MeasurementType {
  KILOGRAMS("kg"), GRAMS("g"), MILLILITRES("ml"), CUPS("cups");
  @Getter
  private String value;
}
