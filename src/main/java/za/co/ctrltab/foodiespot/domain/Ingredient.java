package za.co.ctrltab.foodiespot.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Ingredient {
  private Long id;
  private String name;
  private IngredientType ingredientType;
  private MeasurementType measurementType;
  private int quantity;
  private int calories;
}
