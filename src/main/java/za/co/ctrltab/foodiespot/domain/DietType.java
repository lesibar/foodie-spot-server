package za.co.ctrltab.foodiespot.domain;

public enum DietType {
  NONE, PALEO, BLOOD_TYPE, VEGAN, SOUTH_BEACH, RAW_FOOD, MEDITERRANEAN
}
