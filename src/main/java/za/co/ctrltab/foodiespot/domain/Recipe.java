package za.co.ctrltab.foodiespot.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Recipe {
  private Long id;
  private String name;
  private int preparationTime;
  private Date createdDate;
  private int totalCalories;
  private String imageUrl;
  private Collection<Ingredient> ingredients;
  private Collection<DietType> dietTypes;
  private Collection<Creator> creators;
}
