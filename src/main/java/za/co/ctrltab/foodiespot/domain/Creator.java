package za.co.ctrltab.foodiespot.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Creator {
  private Long id;
  private String name;
  private String emailAddress;
  private int numberOfContributions;
}
