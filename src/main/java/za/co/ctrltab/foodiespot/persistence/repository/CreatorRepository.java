package za.co.ctrltab.foodiespot.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.ctrltab.foodiespot.persistence.entity.CreatorEntity;

import java.util.Optional;

@Repository
public interface CreatorRepository extends JpaRepository<CreatorEntity, Long> {
    Optional<CreatorEntity> findCreatorEntityByName(String name);
}
