package za.co.ctrltab.foodiespot.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.ctrltab.foodiespot.persistence.entity.IngredientEntity;
import java.util.List;
import java.util.Optional;

@Repository
public interface IngredientRepository extends JpaRepository<IngredientEntity, Long> {
  Optional<IngredientEntity> findByName(String name);

  List<IngredientEntity> findByIngredientType(String ingredientType);
}
