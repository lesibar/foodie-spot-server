package za.co.ctrltab.foodiespot.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;
import za.co.ctrltab.foodiespot.domain.DietType;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity(name = "recipe")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecipeEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @CreatedDate
  private Date createdDate;

  private String name;
  private int totalCalories;
  private String imageUrl;
  private int preparationTime;

  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.LAZY)
  @LazyCollection(LazyCollectionOption.FALSE)
  private Collection<DietType> dietTypes;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(name = "recipe_ingredient",
      joinColumns = @JoinColumn(name = "ingredient_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "recipe_id", referencedColumnName = "id"))
  private List<IngredientEntity> ingredients;

  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinTable(name = "recipe_creator",
      joinColumns = @JoinColumn(name = "creator_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "recipe_id", referencedColumnName = "id"))
  private List<CreatorEntity> creators;

}
