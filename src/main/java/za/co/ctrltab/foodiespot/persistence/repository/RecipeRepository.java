package za.co.ctrltab.foodiespot.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.ctrltab.foodiespot.domain.DietType;
import za.co.ctrltab.foodiespot.persistence.entity.RecipeEntity;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface RecipeRepository extends JpaRepository<RecipeEntity, Long> {
  Optional<RecipeEntity> findByName(String name);

  List<RecipeEntity> findByDietTypes(Collection<DietType> dietTypes);
}
