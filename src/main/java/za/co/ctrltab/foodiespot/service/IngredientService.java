package za.co.ctrltab.foodiespot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.ctrltab.foodiespot.component.mapper.IngredientMapper;
import za.co.ctrltab.foodiespot.domain.Ingredient;
import za.co.ctrltab.foodiespot.persistence.repository.IngredientRepository;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IngredientService {

  @Autowired
  private IngredientMapper ingredientMapper;

  @Autowired
  private IngredientRepository ingredientRepository;

  public List<Ingredient> getAllRecipes() {
    return ingredientRepository.findAll().stream().map(ingredientMapper::mapEntityToIngredient)
        .collect(Collectors.toList());
  }

}
