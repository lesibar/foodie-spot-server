package za.co.ctrltab.foodiespot.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.ctrltab.foodiespot.component.mapper.IngredientMapper;
import za.co.ctrltab.foodiespot.component.mapper.RecipeMapper;
import za.co.ctrltab.foodiespot.domain.Ingredient;
import za.co.ctrltab.foodiespot.domain.Recipe;
import za.co.ctrltab.foodiespot.domain.exception.NotFoundException;
import za.co.ctrltab.foodiespot.persistence.entity.RecipeEntity;
import za.co.ctrltab.foodiespot.persistence.repository.RecipeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import static java.lang.String.format;

@Service
@Slf4j
public class RecipeService {

  private static final String RECIPE_NOT_FOUND_EXCEPTION_MESSAGE = "Recipe Not Found";

  @Autowired
  private RecipeRepository recipeRepository;

  @Autowired
  private RecipeMapper recipeMapper;

  @Autowired
  private IngredientMapper ingredientMapper;

  public Recipe findRecipeById(final Long id) {
    log.info("Finding recipe with Id=[{}]", id);
    return getRecipeById(id).stream().map(recipeMapper::mapEntityToRecipe).findFirst()
        .orElseThrow(() -> new NotFoundException(RECIPE_NOT_FOUND_EXCEPTION_MESSAGE));
  }

  public void updateRecipe(final Long id, final Recipe recipe) {
    getRecipeById(id).ifPresentOrElse(
        r -> recipeRepository.save(recipeMapper.mapRecipeToEntity(recipe)),
        throwNotFoundException());
  }

  public void deleteRecipe(final Long id) {
    log.info("Attempting to delete recipe with Id=[{}]", id);
    getRecipeById(id).ifPresentOrElse(r -> recipeRepository.deleteById(r.getId()),
        throwNotFoundException());
  }

  public void createRecipe(final Recipe recipe) {
    log.info("Creating [{}] recipe", recipe.getName());
    recipeRepository.findByName(recipe.getName()).ifPresentOrElse(
        r -> new RuntimeException(format("[%s] Recipe Already Exists", r.getName())),
        () -> recipeRepository.save(recipeMapper.mapRecipeToEntity(recipe)));
  }

  public List<Ingredient> getRecipeIngredients(final Long id) {
    final List<Ingredient> ingredients = new ArrayList<>();

    getRecipeById(id).ifPresentOrElse(
        s -> ingredients.addAll(recipeMapper.mapEntitiesToIngredients(s.getIngredients())),
        throwNotFoundException());
    return ingredients;
  }

  private Optional<RecipeEntity> getRecipeById(final Long id) {
    return recipeRepository.findById(id);
  }

  public void addIngredientToRecipe(final Long id, final Ingredient ingredient) {
    getRecipeById(id).ifPresentOrElse(
        r -> r.getIngredients().add(ingredientMapper.mapIngredientToEntity(ingredient)),
        throwNotFoundException());
  }

  public void removeIngredientFromRecipe(final Long id, final Long ingredientId) {
    getRecipeById(id).ifPresentOrElse(r -> {
      r.getIngredients().removeIf(ie -> ie.getId().equals(ingredientId));
      recipeRepository.save(r);
    }, throwNotFoundException());
  }

  public List<Recipe> getAllRecipes() {
    return recipeRepository.findAll().stream().map(recipeMapper::mapEntityToRecipe)
        .collect(Collectors.toList());
  }

  private Runnable throwNotFoundException() {
    return () -> new NotFoundException(RECIPE_NOT_FOUND_EXCEPTION_MESSAGE);
  }

}
