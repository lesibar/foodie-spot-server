package za.co.ctrltab.foodiespot.persistence.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.ctrltab.foodiespot.persistence.entity.CreatorEntity;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DataJpaTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class CreatorRepositoryTest {

    private static final String NAME = "Lesiba";
    private static final String EMAIL = "lesiba@ctrltab.co.za";

    @Autowired
    private CreatorRepository creatorRepository;

    @Before
    public void init() {
        creatorRepository.save(creatorEntity(0L, NAME, EMAIL));
    }

    @After
    public void cleanUp() {
        creatorRepository.deleteAll();
    }

    @Test
    public void when_LookingUpAnCreator_expect_success() {
        final Optional<CreatorEntity> creator = creatorRepository.findCreatorEntityByName(NAME);
        final CreatorEntity creatorEntity = creator.get();

        assertNotNull(creatorEntity);
        assertEquals(EMAIL, creatorEntity.getEmailAddress());
        assertEquals(NAME, creatorEntity.getName());

    }

    private CreatorEntity creatorEntity(final Long id, final String name, final String email) {
        return CreatorEntity.builder().id(id).name(name).emailAddress(email)
                .build();
    }
}
