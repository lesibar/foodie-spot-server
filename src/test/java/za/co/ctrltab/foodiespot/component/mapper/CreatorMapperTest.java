package za.co.ctrltab.foodiespot.component.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.ctrltab.foodiespot.domain.Creator;
import za.co.ctrltab.foodiespot.persistence.entity.CreatorEntity;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(value = "test")
public class CreatorMapperTest {

  @Autowired
  private CreatorMapper creatorMapper;

  @Test
  public void when_Mapping_creatorDTOtoEntity_expect_success() {
    final Creator creator = creatorMapper.mapEntityToCreator(creatorEntity());

    assertNotNull("Creator Object is not null", creator);
    assertEquals("lesiba@ctrltab.co.za", creator.getEmailAddress());
    assertEquals("Lesiba", creator.getName());
    assertEquals(45L, (long) creator.getId());
  }

  @Test
  public void when_Mapping_EntityToDTO_expect_success() {
    final CreatorEntity creator = creatorMapper.mapCreatorToEntity(creator());

    assertNotNull("Creator Object is not null", creator);
    assertEquals("lesiba@ctrltab.co.za", creator.getEmailAddress());
    assertEquals("Lesiba", creator.getName());
    assertEquals(45L, (long) creator.getId());
  }

  private CreatorEntity creatorEntity() {
    return CreatorEntity.builder().id(45L).name("Lesiba").emailAddress("lesiba@ctrltab.co.za")
        .build();
  }

  private Creator creator() {
    return Creator.builder().id(45L).name("Lesiba").emailAddress("lesiba@ctrltab.co.za").build();
  }
}
